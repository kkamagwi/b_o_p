#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys

import os
import requests
from optparse import OptionParser

BITBUCKET_TOKEN = os.environ.get('BITBUCKET_TOKEN', None)

def get_open_pr_for_user(username, repository):
    print("Grabbing user info for {} username".format(username))

    BITBUCKET_BASE_URL = 'https://bitbucket.org/'

    print("Grabbing PRs data...")
    params = {
        "private_token": BITBUCKET_TOKEN,
    }

    repo_uri = '!api/2.0/repositories/{0}/{1}/pullrequests'
    r = requests.get(BITBUCKET_BASE_URL + repo_uri.format(username, repository), params=params).json()
	# blah
    for pr in r:
        print(pr)

    return len(r)


if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("-u", "--username", dest="username", help="Bitbucket username")
    parser.add_option("-r", "--repository", dest="repository", help="Bitbucket repository")

    (options, args) = parser.parse_args()
    if not options.username or not options.repository:
        parser.print_help()
        sys.exit(0)

    print(get_open_pr_for_user(options.username, options.repository))
